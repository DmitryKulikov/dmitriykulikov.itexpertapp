﻿
namespace ItExpert.BO
{
	public class User: PersistentObject
	{
		public string NickName;

		public int? DepartmentId;

		public string DepartmentName;

		public int? PersonId;

		public string PersonName;

		public int? PositionId;

		public string PositionName;

		public int? SuperUserId;

		public string SuperUserName;
	}
}
