﻿
namespace ItExpert.BO
{
	public class Person: PersistentObject
	{
		public string FirstName;

		public string LastName;

		public string MiddleName;

		public string Email;

		public string PhoneNumber;
	}
}
