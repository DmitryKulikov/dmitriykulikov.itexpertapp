﻿
namespace ItExpert.Sys.Data
{
	using System.Collections.Generic;

	public interface IRepository<TEntity>
	{
		TEntity Get(int id);

		IEnumerable<TEntity> GetAll(int offset, int rowCount);

		int Create(TEntity item);

		void Update(TEntity item);

		void Delete(int id);
	}
}
