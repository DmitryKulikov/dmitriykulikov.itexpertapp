﻿
namespace ItExpert.Sys.Data
{
	using System;
	using System.Data;

	public static class SqlExtentions
	{
		public static void AddParameter(this IDbCommand cmd, object value)
		{
			var p = cmd.CreateParameter();
			p.ParameterName = cmd.Parameters.Count.ToString();
			if (value != null)
				p.Value = value;
			else
				p.Value = DBNull.Value;
			cmd.Parameters.Add(p);
		}

		public static void AddParameter(this IDbCommand cmd, string name, object value)
		{
			var p = cmd.CreateParameter();
			p.ParameterName = name;
			if (value != null)
				p.Value = value;
			else
				p.Value = DBNull.Value;
			cmd.Parameters.Add(p);
		}

		public static int Identity(this IDbCommand cmd)
		{
			cmd.Parameters.Clear();
			cmd.CommandText = "SELECT RowId from LastInsertRowId";
			return Convert.ToInt32(cmd.ExecuteScalar());
		}
	}
}
