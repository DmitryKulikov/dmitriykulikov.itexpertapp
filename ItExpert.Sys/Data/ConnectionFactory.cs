﻿
namespace ItExpert.Sys.Data
{
	using System.Configuration;
	using System.Data;
	using System.Data.Common;

	public class ConnectionFactory
	{
		static readonly DbProviderFactory _factory;
		static readonly string _connectionString;

		static ConnectionFactory()
		{
			var settings = ConfigurationManager.ConnectionStrings["ItExpert"];
			_factory = DbProviderFactories.GetFactory(settings.ProviderName);
			_connectionString = settings.ConnectionString;
		}

		public static IDbConnection GetConnection()
		{
			IDbConnection conn = _factory.CreateConnection();
			conn.ConnectionString = _connectionString;
			return conn;
		}
	}
}
