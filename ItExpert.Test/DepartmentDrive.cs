﻿
namespace ItExpert.Test
{

	using System;
	using System.Linq;

	using ItExpert.BO;
	using ItExpert.Web.Api;
	using ItExpert.Web.Model;

	class DepartmentDrive
	{
		DepartmentsController c = new DepartmentsController();

		public void Run()
		{
			var no = Create();
			Print(no);
			PrintAll();
			Update(no);
			PrintAll();
			Delete(no);
			PrintAll();
		}

		void Print(int id)
		{
			var item = c.Get(id);
			if (item == null)
				Console.WriteLine("Department by ID = {0} is not found", id);
			else
				Console.WriteLine("Department by ID -> {0} {1} {2} {3}",
					item.Id, item.Name, item.CompanyId, item.CompanyName);
			Console.WriteLine("\n");
		}

		void PrintAll()
		{
			var items = c.Get();

			Console.WriteLine("List Departments -> \n");

			if (items == null || items.Count() == 0)
				Console.WriteLine("No Departments found...");
			else
			{
				foreach (var item in items)
					Console.WriteLine("{0} {1} {2} {3}",
						item.Id, item.Name, item.CompanyId, item.CompanyName);
			}
			Console.WriteLine("\n");
		}

		int Create()
		{
			Console.WriteLine("Departments. Create...\n");
			return c.Post(new Department
			{
				Name = Guid.NewGuid().ToString(),
				CompanyId = 6
			});
		}

		void Update(int id)
		{
			Console.WriteLine("Departments. Update...\n");
			c.Put(id, new Department
			{
				Id = id,
				Name = "Some new Department Name, no = " + id,
				CompanyId = 7
			});
		}

		void Delete(int id)
		{
			Console.WriteLine("Departments. Delete...\n");
			c.Delete(id);
		}

	}
}
