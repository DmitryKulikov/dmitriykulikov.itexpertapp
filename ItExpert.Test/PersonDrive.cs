﻿
namespace ItExpert.Test
{

	using System;
	using System.Linq;

	using ItExpert.BO;
	using ItExpert.Web.Api;
	using ItExpert.Web.Model;

	class PersonDrive
	{
		PersonsController c = new PersonsController();

		public void Run()
		{
			var no = Create();
			Print(no);
			PrintAll();
			Update(no);
			PrintAll();
			Delete(no);
			PrintAll();
		}

		void Print(int id)
		{
			var item = c.Get(id);
			if (item == null)
				Console.WriteLine("Person by ID = {0} is not found", id);
			else
				Console.WriteLine(
					"Person by ID -> {0} {1} {2} {3} {4} {5}",
					item.Id,
					item.FirstName,
					item.LastName,
					item.MiddleName,
					item.Email,
					item.PhoneNumber);

			Console.WriteLine("\n");
		}

		void PrintAll()
		{
			var items = c.Get();

			Console.WriteLine("List Persons -> \n");

			if (items == null || items.Count() == 0)
				Console.WriteLine("No Persons found...");
			else
			{
				foreach (var item in items)
					Console.WriteLine(
						"{0} {1} {2} {3} {4} {5}",
						item.Id,
						item.FirstName,
						item.LastName,
						item.MiddleName,
						item.Email,
						item.PhoneNumber);
			}
			Console.WriteLine("\n");
		}

		int Create()
		{
			Console.WriteLine("Persons. Create...\n");
			return c.Post(new Person
			{
				FirstName = Guid.NewGuid().ToString(),
				LastName = Guid.NewGuid().ToString(),
				MiddleName = Guid.NewGuid().ToString(),
				Email = "xxx@xx.com",
				PhoneNumber = "123-456-789-0"
			});
		}

		void Update(int id)
		{
			Console.WriteLine("Persons. Update...\n");
			c.Put(id, new Person
			{
				Id = id,
				FirstName = "A new Person FirstName, " + id,
				LastName = "A new Person LastName, " + id,
				MiddleName = "A new Person MiddleName, " + id,
				Email = "yyy@yy.com",
				PhoneNumber = "0-987-654-321"
			});
		}

		void Delete(int id)
		{
			Console.WriteLine("Persons. Delete...\n");
			c.Delete(id);
		}

	}
}
