﻿
namespace ItExpert.Test
{

	using System;
	using System.Linq;

	using ItExpert.BO;
	using ItExpert.Web.Api;
	using ItExpert.Web.Model;

	class UserDrive
	{
		UsersController c = new UsersController();

		public void Run()
		{
			var no = Create();
			Print(no);
			PrintAll();
			Update(no);
			PrintAll();
			Delete(no);
			PrintAll();
		}

		void Print(int id)
		{
			var item = c.Get(id);
			if (item == null)
				Console.WriteLine("User by ID = {0} is not found", id);
			else
				Console.WriteLine(
					"User by ID -> {0} {1} {2} {3} {4} {5} {6} {7} {8} {9}",
					item.Id,
					item.NickName,
					item.DepartmentId,
					item.DepartmentName,
					item.PersonId,
					item.PersonName,
					item.PositionId,
					item.PositionName,
					item.SuperUserId,
					item.SuperUserName);

			Console.WriteLine("\n");
		}

		void PrintAll()
		{
			var items = c.Get();

			Console.WriteLine("List Users -> \n");

			if (items == null || items.Count() == 0)
				Console.WriteLine("No Users found...");
			else
			{
				foreach (var item in items)
					Console.WriteLine(
						"{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}",
						item.Id,
						item.NickName,
						item.DepartmentId,
						item.DepartmentName,
						item.PersonId,
						item.PersonName,
						item.PositionId,
						item.PositionName,
						item.SuperUserId,
						item.SuperUserName);
			}
			Console.WriteLine("\n");
		}

		int Create()
		{
			Console.WriteLine("Users. Create...\n");
			return c.Post(new User
			{
				NickName = Guid.NewGuid().ToString(),
				DepartmentId = 1,
				PersonId = 1
			});
		}

		void Update(int id)
		{
			Console.WriteLine("Users. Update...\n");
			c.Put(id, new User
			{
				Id = id,
				NickName = "A new User NickName, " + id
			});
		}

		void Delete(int id)
		{
			Console.WriteLine("Users. Delete...\n");
			c.Delete(id);
		}

	}
}
