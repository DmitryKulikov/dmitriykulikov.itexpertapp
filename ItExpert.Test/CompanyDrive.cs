﻿
namespace ItExpert.Test
{

	using System;
	using System.Linq;

	using ItExpert.BO;
	using ItExpert.Web.Api;
	using ItExpert.Web.Model;

	class CompanyDrive
	{
		CompaniesController c = new CompaniesController();

		public void Run()
		{
			var no = Create();
			Print(no);
			PrintAll();
			Update(no);
			PrintAll();
			Delete(no);
			PrintAll();
		}

		void Print(int id)
		{
			var item = c.Get(id);
			if (item == null)
				Console.WriteLine("Company by ID = {0} is not found", id);
			else
				Console.WriteLine("Company by ID -> {0} {1} {2}", item.Id, item.Name, item.Logo);
			Console.WriteLine("\n");
		}

		void PrintAll()
		{
			var items = c.Get();

			Console.WriteLine("List Companies -> \n");

			if (items == null || items.Count() == 0)
				Console.WriteLine("No Companies found...");
			else
			{
				foreach (var i in items)
					Console.WriteLine("{0} {1} {2}", i.Id, i.Name, i.Logo);
			}
			Console.WriteLine("\n");
		}

		int Create()
		{
			Console.WriteLine("Companies. Create...\n");
			return c.Post(new Company
			{
				Name = Guid.NewGuid().ToString(),
				Logo = Guid.NewGuid().ToString()
			});
		}

		void Update(int id)
		{
			Console.WriteLine("Companies. Update...\n");
			c.Put(id, new Company
			{
				Id = id,
				Name = "Some new Name, no = " + id,
				Logo = "Some new Logo, no = " + id
			});
		}

		void Delete(int id)
		{
			Console.WriteLine("Companies. Delete...\n");
			c.Delete(id);
		}

	}
}
