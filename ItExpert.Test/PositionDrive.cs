﻿
namespace ItExpert.Test
{

	using System;
	using System.Linq;

	using ItExpert.BO;
	using ItExpert.Web.Api;
	using ItExpert.Web.Model;

	class PositionDrive
	{
		PositionsController c = new PositionsController();

		public void Run()
		{
			var no = Create();
			Print(no);
			PrintAll();
			Update(no);
			PrintAll();
			Delete(no);
			PrintAll();
		}

		void Print(int id)
		{
			var item = c.Get(id);
			if (item == null)
				Console.WriteLine("Position by ID = {0} is not found", id);
			else
				Console.WriteLine("Position by ID -> {0} {1} {2}", item.Id, item.Name, item.Salary);
			Console.WriteLine("\n");
		}

		void PrintAll()
		{
			var items = c.Get();

			Console.WriteLine("List positions -> \n");

			if (items == null || items.Count() == 0)
				Console.WriteLine("No positions found...");
			else
			{
				foreach (var i in items)
					Console.WriteLine("{0} {1} {2}", i.Id, i.Name, i.Salary);
			}
			Console.WriteLine("\n");
		}

		int Create()
		{
			Console.WriteLine("Positions. Create...\n");
			return c.Post(new Position
			{
				Name = Guid.NewGuid().ToString(),
				Salary = new decimal(15.5)
			});
		}

		void Update(int id)
		{
			Console.WriteLine("Positions. Update...\n");
			c.Put(id, new Position
			{
				Id = id,
				Name = "Some new Name, no = " + id,
				Salary = new decimal(-20)
			});
		}

		void Delete(int id)
		{
			Console.WriteLine("Positions. Delete...\n");
			c.Delete(id);
		}

	}
}
