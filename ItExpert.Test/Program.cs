﻿
namespace ItExpert.Test
{

	using System;

	using ItExpert.BL;
	using ItExpert.BO;

	class Program
	{
		static void Main(string[] args)
		{

			new PositionDrive().Run();

			new CompanyDrive().Run();

			new DepartmentDrive().Run();

			new PersonDrive().Run();

			new UserDrive().Run();

		}
	}
}
