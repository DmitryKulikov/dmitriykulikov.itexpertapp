﻿
namespace ItExpert.BL
{
	using System;
	using System.Collections.Generic;

	using ItExpert.BO;
	using ItExpert.Sys.Data;

	public class PersonRepository: IRepository<Person>
	{
		public Person Get(int id)
		{
			Person result = null;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = 
@"SELECT FirstName, LastName, MiddleName, Email, PhoneNumber FROM Persons WHERE id=@0";
					cmd.AddParameter(id);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
						{
							result = new Person()
							{
								Id = id,
								FirstName = reader.GetString(0),
								LastName = reader.GetString(1),
								MiddleName = reader.GetString(2),
								Email = reader.GetString(3)
							};
							if (!reader.IsDBNull(4))
								result.PhoneNumber = reader.GetString(4);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public IEnumerable<Person> GetAll(int offset, int rowCount)
		{
			var result = new List<Person>();
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"SELECT Id, FirstName, LastName, MiddleName, Email, PhoneNumber FROM Persons LIMIT @0, @1";

					cmd.AddParameter(offset);
					cmd.AddParameter(rowCount);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						Person item;
						while (reader.Read())
						{
							item = new Person()
							{
								Id = reader.GetInt32(0),
								FirstName = reader.GetString(1),
								LastName = reader.GetString(2),
								MiddleName = reader.GetString(3),
								Email = reader.GetString(4)
							};
							if (!reader.IsDBNull(5))
								item.PhoneNumber = reader.GetString(5);

							result.Add(item);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public int Create(Person item)
		{
			int identity = -1;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"INSERT INTO Persons (FirstName, LastName, MiddleName, Email, PhoneNumber)
	VALUES (@0, @1, @2, @3, @4);
SELECT RowId from LastInsertRowId;";
					//
					cmd.AddParameter(item.FirstName);
					cmd.AddParameter(item.LastName);
					cmd.AddParameter(item.MiddleName);
					cmd.AddParameter(item.Email);
					cmd.AddParameter(item.PhoneNumber);
					//
					conn.Open();
					identity = Convert.ToInt32(cmd.ExecuteScalar());
					item.Id = identity;
				}
				conn.Close();
			}
			return identity;
		}

		public void Update(Person item)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"UPDATE Persons SET FirstName=@1, LastName=@2, MiddleName=@3, Email=@4, PhoneNumber=@5
	WHERE Id=@0";
					//
					cmd.AddParameter(item.Id);
					cmd.AddParameter(item.FirstName);
					cmd.AddParameter(item.LastName);
					cmd.AddParameter(item.MiddleName);
					cmd.AddParameter(item.Email);
					cmd.AddParameter(item.PhoneNumber);
					//
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}

		public void Delete(int id)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "DELETE FROM Persons WHERE id=" + id.ToString();
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}
	}
}
