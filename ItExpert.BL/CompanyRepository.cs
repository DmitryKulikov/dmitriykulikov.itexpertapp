﻿
namespace ItExpert.BL
{
	using ItExpert.BO;
	using ItExpert.Sys.Data;
	using System;
	using System.Collections.Generic;

	public class CompanyRepository: IRepository<Company>
	{
		public Company Get(int id)
		{
			Company result = null;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "SELECT Name, Logo FROM Companies WHERE id=@0";
					cmd.AddParameter(id);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
						{
							result = new Company()
							{
								Id = id,
								Name = reader.GetString(0),
								Logo = reader.GetString(1)
							};
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public IEnumerable<Company> GetAll(int offset, int rowCount)
		{
			var result = new List<Company>(50);
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "SELECT Id, Name, Logo FROM Companies LIMIT @0, @1;";
					cmd.AddParameter(offset);
					cmd.AddParameter(rowCount);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						Company item;
						while (reader.Read())
						{

							item = new Company()
							{
								Id = reader.GetInt32(0),
								Name = reader.GetString(1),
								Logo = reader.GetString(2)
							};
							result.Add(item);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public int Create(Company item)
		{
			int identity = -1;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"INSERT INTO Companies (Name, Logo) VALUES (@0, @1);
SELECT RowId from LastInsertRowId;";
					//
					cmd.AddParameter(item.Name);
					cmd.AddParameter(item.Logo);
					//
					conn.Open();
					identity = Convert.ToInt32(cmd.ExecuteScalar());
					item.Id = identity;
				}
				conn.Close();
			}
			return identity;
		}

		public void Update(Company item)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "UPDATE Companies SET Name=@1, Logo=@2 WHERE Id=@0";
					//
					cmd.AddParameter(item.Id);
					cmd.AddParameter(item.Name);
					cmd.AddParameter(item.Logo);
					//
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}

		public void Delete(int id)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "DELETE FROM Companies WHERE id=" + id.ToString();
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}
	}
}
