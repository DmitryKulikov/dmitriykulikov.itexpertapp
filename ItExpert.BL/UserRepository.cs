﻿
namespace ItExpert.BL
{
	using System;
	using System.Collections.Generic;

	using ItExpert.BO;
	using ItExpert.Sys.Data;

	public class UserRepository: IRepository<User>
	{
		public User Get(int id)
		{
			User result= null;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"SELECT us.Id,
		us.NickName,
		us.DepartmentId,
		d.Name as DepartmentName,
		us.PersonId,
		(p.FirstName || ' ' || p.LastName) as PersonName,
		us.PositionId,
		pos.Name as PositionName,
		us.SuperUserId,
		su.NickName as SuperUserNickName
	FROM Users us
	LEFT OUTER JOIN Departments d on d.Id=us.DepartmentId
	LEFT OUTER JOIN Persons p on p.Id=us.PersonId
	LEFT OUTER JOIN Positions pos on pos.Id=us.PositionId
	LEFT OUTER JOIN Users su on su.Id=us.SuperUserId
	WHERE us.Id=@0";
					cmd.AddParameter(id);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
						{

							result = new User()
							{
								Id = reader.GetInt32(0),
								NickName = reader.GetString(1)
							};

							if (!reader.IsDBNull(2)) result.DepartmentId = reader.GetInt32(2);
							if (!reader.IsDBNull(3)) result.DepartmentName = reader.GetString(3);

							if (!reader.IsDBNull(4)) result.PersonId = reader.GetInt32(4);
							if (!reader.IsDBNull(5)) result.PersonName = reader.GetString(5);

							if (!reader.IsDBNull(6)) result.PositionId = reader.GetInt32(6);
							if (!reader.IsDBNull(7)) result.PositionName = reader.GetString(7);

							if (!reader.IsDBNull(8)) result.SuperUserId = reader.GetInt32(8);
							if (!reader.IsDBNull(9)) result.SuperUserName = reader.GetString(9);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public IEnumerable<User> GetAll(int offset, int rowCount)
		{
			var result = new List<User>(50);
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"SELECT us.Id,
		us.NickName,
		us.DepartmentId,
		d.Name as DepartmentName,
		us.PersonId,
		(p.FirstName || ' ' || p.LastName) as PersonName,
		us.PositionId,
		pos.Name as PositionName,
		us.SuperUserId,
		su.NickName as SuperUserNickName
	FROM Users us
	LEFT OUTER JOIN Departments d on d.Id=us.DepartmentId
	LEFT OUTER JOIN Persons p on p.Id=us.PersonId
	LEFT OUTER JOIN Positions pos on pos.Id=us.PositionId
	LEFT OUTER JOIN Users su on su.Id=us.SuperUserId
	LIMIT @0, @1";
					cmd.AddParameter(offset);
					cmd.AddParameter(rowCount);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						User item;
						while (reader.Read())
						{

							item = new User()
							{
								Id = reader.GetInt32(0),
								NickName = reader.GetString(1)
							};

							if (!reader.IsDBNull(2)) item.DepartmentId = reader.GetInt32(2);
							if (!reader.IsDBNull(3)) item.DepartmentName = reader.GetString(3);

							if (!reader.IsDBNull(4)) item.PersonId = reader.GetInt32(4);
							if (!reader.IsDBNull(5)) item.PersonName = reader.GetString(5);

							if (!reader.IsDBNull(6)) item.PositionId = reader.GetInt32(6);
							if (!reader.IsDBNull(7)) item.PositionName = reader.GetString(7);

							if (!reader.IsDBNull(8)) item.SuperUserId = reader.GetInt32(8);
							if (!reader.IsDBNull(9)) item.SuperUserName = reader.GetString(9);

							result.Add(item);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public int Create(User item)
		{
			int identity = -1;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"INSERT INTO Users (NickName, DepartmentId, PersonId, PositionId, SuperUserId)
	VALUES (@0, @1, @2, @3, @4);
SELECT RowId from LastInsertRowId;";
					//
					cmd.AddParameter(item.NickName);
					cmd.AddParameter(item.DepartmentId);
					cmd.AddParameter(item.PersonId);
					cmd.AddParameter(item.PositionId);
					cmd.AddParameter(item.SuperUserId);
					//
					conn.Open();
					identity = Convert.ToInt32(cmd.ExecuteScalar());
					item.Id = identity;
				}
				conn.Close();
			}
			return identity;
		}

		public void Update(User item)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"UPDATE Users SET NickName=@1, DepartmentId=@2, PersonId=@3, PositionId=@4, SuperUserId=@5
	WHERE Id=@0";
					//
					cmd.AddParameter(item.Id);
					cmd.AddParameter(item.NickName);
					cmd.AddParameter(item.DepartmentId);
					cmd.AddParameter(item.PersonId);
					cmd.AddParameter(item.PositionId);
					cmd.AddParameter(item.SuperUserId);
					//
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}

		public void Delete(int id)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "DELETE FROM Users WHERE id=" + id.ToString();
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}
	}
}
