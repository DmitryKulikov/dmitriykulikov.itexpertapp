﻿
namespace ItExpert.BL
{
	using System;
	using System.Collections.Generic;

	using ItExpert.BO;
	using ItExpert.Sys.Data;
	
	public class DepartmentRepository: IRepository<Department>
	{
		public Department Get(int id)
		{
			Department result = null;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"SELECT d.Name, c.Id, c.Name
	FROM Departments d
	LEFT OUTER JOIN Companies c on c.Id=d.CompanyId
	WHERE d.Id=@0";
					cmd.AddParameter(id);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
						{
							result = new Department()
							{
								Id = id,
								Name = reader.GetString(0),
								CompanyId = reader.GetInt32(1),
								CompanyName = reader.GetString(2)
							};
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public IEnumerable<Department> GetAll(int offset, int rowCount)
		{
			var result = new List<Department>(50);
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"SELECT d.Id, d.Name, c.Id, c.Name
	FROM Departments d
	LEFT OUTER JOIN Companies c on c.Id=d.CompanyId
	LIMIT @0, @1;";
					cmd.AddParameter(offset);
					cmd.AddParameter(rowCount);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						Department item;
						while (reader.Read())
						{

							item = new Department()
							{
								Id = reader.GetInt32(0),
								Name = reader.GetString(1),
								CompanyId = reader.GetInt32(2),
								CompanyName = reader.GetString(3)
							};
							result.Add(item);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public int Create(Department item)
		{
			int identity = -1;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"INSERT INTO Departments (Name, CompanyId) VALUES (@0, @1);
SELECT RowId from LastInsertRowId;";
					//
					cmd.AddParameter(item.Name);
					cmd.AddParameter(item.CompanyId);
					//
					conn.Open();
					identity = Convert.ToInt32(cmd.ExecuteScalar());
					item.Id = identity;
				}
				conn.Close();
			}
			return identity;
		}

		public void Update(Department item)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "UPDATE Departments SET Name=@1, CompanyId=@2 WHERE Id=@0";
					//
					cmd.AddParameter(item.Id);
					cmd.AddParameter(item.Name);
					cmd.AddParameter(item.CompanyId);
					//
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}

		public void Delete(int id)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "DELETE FROM Departments WHERE id=" + id.ToString();
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}
	}
}
