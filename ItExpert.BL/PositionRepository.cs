﻿
namespace ItExpert.BL
{
	using ItExpert.BO;
	using ItExpert.Sys.Data;
	using System;
	using System.Collections.Generic;

	public class PositionRepository: IRepository<Position>
	{
		public Position Get(int id)
		{
			Position result = null;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "SELECT Name, Salary FROM Positions WHERE id=@0";
					cmd.AddParameter(id);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
						{
							result = new Position()
							{
								Id = id,
								Name = reader.GetString(0)
							};
							if (!reader.IsDBNull(1))
								result.Salary = reader.GetDecimal(1);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public IEnumerable<Position> GetAll(int offset, int rowCount)
		{
			var result = new List<Position>(50);
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "SELECT Id, Name, Salary FROM Positions LIMIT @0, @1;";
					cmd.AddParameter(offset);
					cmd.AddParameter(rowCount);
					conn.Open();
					using (var reader = cmd.ExecuteReader())
					{
						Position item;
						while (reader.Read())
						{

							item = new Position()
							{
								Id = reader.GetInt32(0),
								Name = reader.GetString(1)
							};
							if (!reader.IsDBNull(2))
								item.Salary = reader.GetDecimal(2);
							result.Add(item);
						}
						reader.Close();
					}
				}
				conn.Close();
			}
			return result;
		}

		public int Create(Position item)
		{
			int identity = -1;
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText =
@"INSERT INTO Positions (Name, Salary) VALUES (@0, @1);
SELECT RowId from LastInsertRowId;";
					//
					cmd.AddParameter(item.Name);
					cmd.AddParameter(item.Salary);
					//
					conn.Open();
					identity = Convert.ToInt32(cmd.ExecuteScalar());
					item.Id = identity;
				}
				conn.Close();
			}
			return identity;
		}

		public void Update(Position item)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "UPDATE Positions SET Name=@1, Salary=@2 WHERE Id=@0";
					//
					cmd.AddParameter(item.Id);
					cmd.AddParameter(item.Name);
					cmd.AddParameter(item.Salary);
					//
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}

		public void Delete(int id)
		{
			using (var conn = ConnectionFactory.GetConnection())
			{
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "DELETE FROM Positions WHERE id=" + id.ToString();
					conn.Open();
					cmd.ExecuteNonQuery();
				}
				conn.Close();
			}
		}
	}
}
