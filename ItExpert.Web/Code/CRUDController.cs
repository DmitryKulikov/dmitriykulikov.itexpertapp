﻿
namespace ItExpert.Web
{
	using System.Collections.Generic;
	using System.Web.Http;

	using ItExpert.Web.Model;
	using ItExpert.Sys.Data;

	public abstract class CRUDController<T>: ApiController
	{
		protected abstract IRepository<T> Repo { get; }

		public IEnumerable<T> Get()
		{
			return Repo.GetAll(0, 50);
		}

		public T Get(int id)
		{
			return Repo.Get(id);
		}

		public int Post([FromBody]T item)
		{
			return Repo.Create(item);
		}

		public void Put(int id, [FromBody]T item)
		{
			Repo.Update(item);
		}

		public void Delete(int id)
		{
			Repo.Delete(id);
		}
	}
}