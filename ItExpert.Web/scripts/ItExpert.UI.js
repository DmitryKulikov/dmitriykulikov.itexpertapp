﻿
(function (w) {

	w.ItExpert.UI = {};

	var grid = (function () {

		function _(cfg) {

			var me = this;
			me.cfg = cfg;
			me.container = document.querySelector(cfg.selector);

			$.ajax({
				method: 'get',
				url: cfg.getUrl,
				success: function (r) {
					me.Render(r);
				},
				error: function () {
					ItExpert.Alert('Сервис не доступен!', 'danger');
				}
			});

			this.AddCreateBtn(me.container, cfg);
		}

		_.prototype.AddCreateBtn = function (container, cfg) {

			var el = document.createElement('div');
			container.appendChild(el);

			el = el.appendChild(document.createElement('button'));

			el.className = 'btn btn-default';
			el.type = 'button';
			el.innerHTML = 'Создать';
			el.onclick = function () {
				ItExpert.LoadView(cfg.editUrl);
			}

			container.appendChild(document.createElement('br'));
		};

		_.prototype.Render = function render(items) {

			var fields = this.cfg.fields,
				i,
				field,
				table = document.createElement('div'),
				thead = document.createElement('thead'),
				_html;

			table.className = 'table-responsive';
			this.container.appendChild(table);

			table = table.appendChild(document.createElement('table'));

			table.className = 'table table-striped';

			_html = '<tr>';
			for (i = 0; field = fields[i]; i++) {
				_html += '<th>' + field.caption + '</th>';
			}
			_html += '<th>#</th>';
			_html += '</tr>';
			thead.innerHTML = _html;

			table.appendChild(thead);

			table = table.appendChild(document.createElement('tbody'));

			var tr, td, a, k, item;

			for (k = 0; item = items[k]; k++) {

				tr = document.createElement('tr');

				for (i = 0; field = fields[i]; i++) {
					td = document.createElement('td');
					td.innerHTML = item[field.name];
					tr.appendChild(td);
				}

				td = document.createElement('td');
				a = document.createElement('a');
				a.innerHTML = 'ред.';
				a.setAttribute('data-id', item.Id);
				a.setAttribute('data-url', this.cfg.editUrl);
				a.onclick = this.editItem;
				td.appendChild(a);

				td.appendChild(document.createTextNode(' / '));

				a = document.createElement('a');
				a.innerHTML = 'уд.';
				a.setAttribute('data-id', item.Id);
				a.setAttribute('data-url', this.cfg.deleteUrl);
				a.onclick = this.removeItem;
				td.appendChild(a);

				tr.appendChild(td);

				table.appendChild(tr);
			}

			this.grid = table;
		};

		_.prototype.editItem = function () {
			ItExpert.LoadView(this.getAttribute('data-url'), { id: this.getAttribute('data-id') });
		};

		_.prototype.removeItem = function () {
			var row = this.parentElement.parentElement;
			$.ajax({
				method: 'delete',
				url: this.getAttribute('data-url') + this.getAttribute('data-id'),
				success: function () {
					row.parentElement.removeChild(row);
					ItExpert.Alert('Запись удалена!', 'success');
				},
				error: function () {
					ItExpert.Alert('Ошибка! Запись не удалена.', 'danger');
				}
			});
		};

		return _;

	})();

	var editForm = (function () {

		function _(cfg) {

			var me = this,
				id = ItExpert.Request('id');

			me.cfg = cfg;
			me.container = document.querySelector(cfg.selector);

			this.Render(me.container, cfg);

			if (id) {

				//Edit entity

				$.ajax({
					method: 'get',
					url: cfg.getUrl + id,
					success: function (r) {
						var elems = me.form.elements;
						for (var i in r) {
							if (elems[i])
								elems[i].value = r[i];
						}
					},
					error: function () {
						ItExpert.LoadView(cfg.redirectUrl);
					}
				});

			}

		}

		_.prototype.Render = function (container, cfg) {

			var el = document.createElement('div');
			container.appendChild(el);

			el.className = 'panel panel-default';

			var heading = document.createElement('div');
			heading.className = 'panel-heading';
			heading.innerHTML = '<h3 class="panel-title">Заполните поля:</h3>';
			el.appendChild(heading);

			el = el.appendChild(document.createElement('div'));
			el.className = 'panel-body';
			el.style.maxWidth = '600px';

			var form = document.createElement('form'),
				i, field, div, input, label;

			input = document.createElement('input');
			input.type = 'hidden';
			input.name = 'Id';

			form.appendChild(input);

			for (i = 0; field = cfg.fields[i]; i++) {

				div = document.createElement('div');
				div.className = 'form-group';
				form.appendChild(div);

				label = document.createElement('label');
				label.className = 'control-label';
				label.innerHTML = field.label;

				div.appendChild(label);

				div = div.appendChild(document.createElement('div'));
				input = document.createElement(field.elType);
				input.className = 'form-control';
				input.name = field.name;
				if (field.onInit)
					field.onInit(input);
				div.appendChild(input);
			}

			input = document.createElement('input');
			input.type = 'submit';
			input.className = 'btn btn-primary';
			input.value = 'Сохранить';

			form.appendChild(input);

			form.cfg = cfg;

			form.onsubmit = this.onSubmit;

			this.form = form;
			el.appendChild(form);
		};

		_.prototype.onSubmit = function (e) {

			e.preventDefault();

			var me = this,
				id = this.elements['Id'].value,
				data = {},
				cfg = {
					url: this.cfg.action,
					contentType: 'application/json',
					success: function () {
						ItExpert.Alert('Запись сохранена!', 'success');
						ItExpert.LoadView(me.cfg.redirectUrl);
					},
					error: function () {
						ItExpert.Alert('Ошибка! Запись не сохранена', 'danger');
					}
				};

			for (var i = 0, el; el = this.elements[i]; i++) {
				data[el.name] = el.value;
			}

			cfg.data = JSON.stringify(data);

			if (id) {
				cfg.method = 'put';
				cfg.url += id;
			}
			else {
				cfg.method = 'post';
			}

			$.ajax(cfg);

		};

		return _;

	})();

	w.ItExpert.UI.Grid = grid;
	w.ItExpert.UI.Form = editForm;

})(window);