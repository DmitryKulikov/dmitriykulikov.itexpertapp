﻿
(function () {

	var ItExpertApp = angular.module('ItExpertApp', ['ngRoute', 'angular-growl']),
		mainNavLinks = document.querySelectorAll('#app-main-nav a');

	function onLoadView() {
		for (var i = mainNavLinks.length; i--;) {
			mainNavLinks[i].classList.remove('active');
		}
		this.classList.add('active');
	};

	for (var i = mainNavLinks.length; i--;) {
		mainNavLinks[i].onclick = onLoadView;
	}

	mainNavLinks[0].classList.add('active');

	ItExpertApp.config(function ($routeProvider, growlProvider) {

		$routeProvider

			.when('/', {
				templateUrl: 'views/grid.html',
				controller: 'usersController',
				title: 'Пользователи'
			})

			.when('/persons', {
				templateUrl: 'views/grid.html',
				controller: 'personsController',
				title: 'Менеджеры'
			})

			.when('/companies', {
				templateUrl: 'views/grid.html',
				controller: 'companiesController',
				title: 'Организации'
			})

			.when('/departments', {
				templateUrl: 'views/grid.html',
				controller: 'departmentsController',
				title: 'Департаменты'
			})

			.when('/positions', {
				templateUrl: 'views/grid.html',
				controller: 'positionsController',
				title: 'Вакансии'
			})

			.when('/users/edit/:id?', {
				templateUrl: 'views/userForm.html',
				controller: 'userController',
				title: 'Вакансия'
			})

			.when('/persons/edit/:id?', {
				templateUrl: 'views/form.html',
				controller: 'personController',
				title: 'Вакансия'
			})

			.when('/companies/edit/:id?', {
				templateUrl: 'views/form.html',
				controller: 'companyController',
				title: 'Вакансия'
			})

			.when('/departments/edit/:id?', {
				templateUrl: 'views/departmentForm.html',
				controller: 'departmentController',
				title: 'Вакансия'
			})

			.when('/positions/edit/:id?', {
				templateUrl: 'views/form.html',
				controller: 'positionController',
				title: 'Вакансия'
			});

		growlProvider.globalTimeToLive(3000);

	});

	ItExpertApp.run(['$rootScope', function ($rootScope) {

		$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
			$rootScope.title = current.$$route.title;
		});

	}]);

	function gridCtrl(
		ctrl,
		$rootScope,
		$scope,
		$http,
		growl,
		cfg) {

		$scope.fields = cfg.fields;
		$scope.editUrl = cfg.editUrl;

		$http.get(cfg.getUrl).success(function (data) {
			$scope.data = data;
		});

		$scope.removeItem = function (index, item) {
			$http({
				method: 'DELETE',
				url: cfg.deleteUrl + item.Id
			})
			.success(function () {
				growl.success('Запись удалена!');
				$scope.data.splice(index, 1);
			})
			.error(function () {
				growl.error('Ошибка! Запись не удалена.');
			});
		}

	};

	function formCtrl(
		ctrl,
		$rootScope,
		$scope,
		$routeParams,
		$http,
		growl,
		cfg) {

		var id = $routeParams.id;

		$scope.model = $scope.model || {};
		$scope.fields = cfg.fields;
		$scope.submit = function () {
			var _cfg = {
				url: cfg.action,
				contentType: 'application/json',
				data: JSON.stringify($scope.model)
			};

			if (id) {
				_cfg.method = 'PUT';
				_cfg.url += id;
			}
			else {
				_cfg.method = 'POST';
			}

			$http(_cfg)
			.success(function () {
				growl.success('Запись сохранена!');
				document.location.hash = cfg.redirectUrl;
			})
			.error(function () {
				growl.error('Ошибка! Запись не сохранена');
			});

		};

		if (id) {
			$http.get(cfg.action + id)
				.success(function (data) {
					$scope.model = data;
				}).error(function () {
					document.location.hash = cfg.redirectUrl;
				});
		}

	};


	ItExpertApp.controller('usersController',
		['$rootScope', '$scope', '$http', 'growl', function ($rootScope, $scope, $http, growl) {

			gridCtrl(
				this,
				$rootScope,
				$scope,
				$http,
				growl,
				{
					getUrl: '/api/users',
					editUrl: '/users/edit',
					deleteUrl: '/api/users/',
					fields: [
						{ caption: 'Ник', name: 'NickName' },
						{ caption: 'Менеджер', name: 'PersonName' },
						{ caption: 'Департамент', name: 'DepartmentName' },
						{ caption: 'Должность', name: 'PositionName' },
						{ caption: 'Супер-пользователь', name: 'SuperUserName' }
					]
				});

		}]);

	ItExpertApp.controller('userController',
		['$rootScope', '$scope', '$routeParams', '$http', '$q', 'growl',
		function ($rootScope, $scope, $routeParams, $http, $q, growl) {

			var me = this;

			$q.all([$http.get('/api/persons'),
				$http.get('/api/departments'),
				$http.get('/api/positions'),
				$http.get('/api/users')])
				.then(function (data) {
					$scope.Persons = data[0].data;
					$scope.Departments = data[1].data;
					$scope.Positions = data[2].data;
					$scope.Users = data[3].data;
					formCtrl(
						me,
						$rootScope,
						$scope,
						$routeParams,
						$http,
						growl,
						{
							action: '/api/users/',
							redirectUrl: '/'
						});
				});

		}]);

	ItExpertApp.controller('personsController',
		['$rootScope', '$scope', '$http', 'growl', function ($rootScope, $scope, $http, growl) {

			gridCtrl(
				this,
				$rootScope,
				$scope,
				$http,
				growl,
				{
					getUrl: '/api/persons',
					editUrl: '/persons/edit',
					deleteUrl: '/api/persons/',
					fields: [
						{ caption: 'Имя', name: 'FirstName' },
						{ caption: 'Фамилия', name: 'LastName' },
						{ caption: 'Псевдоним', name: 'MiddleName' },
						{ caption: 'Email', name: 'Email' },
						{ caption: 'Тел.', name: 'PhoneNumber' }
					]
				});

		}]);

	ItExpertApp.controller('personController',
		['$rootScope', '$scope', '$routeParams', '$http', 'growl',
		function ($rootScope, $scope, $routeParams, $http, growl) {

			formCtrl(
				this,
				$rootScope,
				$scope,
				$routeParams,
				$http,
				growl,
				{
					action: '/api/persons/',
					redirectUrl: '/persons',
					fields: [
						{ elType: 'input', type: 'text', name: 'FirstName', label: 'Имя:', required: true },
						{ elType: 'input', type: 'text', name: 'LastName', label: 'Фамилия:', required: true },
						{ elType: 'input', type: 'text', name: 'MiddleName', label: 'Псевдоним:', required: true },
						{ elType: 'input', type: 'email', name: 'Email', label: 'Email:', required: true },
						{ elType: 'input', type: 'text', name: 'PhoneNumber', label: 'Телефон:', required: true }
					]
				});
	}]);

	ItExpertApp.controller('companiesController',
		['$rootScope', '$scope', '$http', 'growl', function ($rootScope, $scope, $http, growl) {

			gridCtrl(
				this,
				$rootScope,
				$scope,
				$http,
				growl,
				{
					getUrl: '/api/companies',
					editUrl: '/companies/edit',
					deleteUrl: '/api/companies/',
					fields: [
						{ caption: 'Название', name: 'Name' },
						{ caption: 'Логотип', name: 'Logo' }
					]
				});

		}]);

	ItExpertApp.controller('companyController',
		['$rootScope', '$scope', '$routeParams', '$http', 'growl',
		function ($rootScope, $scope, $routeParams, $http, growl) {

			formCtrl(
				this,
				$rootScope,
				$scope,
				$routeParams,
				$http,
				growl,
				{
					action: '/api/companies/',
					redirectUrl: '/companies',
					fields: [
						{ elType: 'input', type: 'text', name: 'Name', label: 'Название:', required: true },
						{ elType: 'input', type: 'text', name: 'Logo', label: 'Логотип:', required: true }
					]
				});
		}]);

	ItExpertApp.controller('departmentsController',
		['$rootScope', '$scope', '$http', 'growl',
		function ($rootScope, $scope, $http, growl) {

			gridCtrl(
				this,
				$rootScope,
				$scope,
				$http,
				growl,
				{
					getUrl: '/api/departments',
					editUrl: '/departments/edit',
					deleteUrl: '/api/departments/',
					fields: [
						{ caption: 'Название', name: 'Name' },
						{ caption: 'Организация', name: 'CompanyName' }
					]
				});

		}]);

	ItExpertApp.controller('departmentController',
		['$rootScope', '$scope', '$routeParams', '$http', 'growl',
		function ($rootScope, $scope, $routeParams, $http, growl) {

			var me = this;

			$http.get('/api/companies').success(function (data) {
				$scope.Companies = data;
				formCtrl(
					me,
					$rootScope,
					$scope,
					$routeParams,
					$http,
					growl,
					{
						action: '/api/departments/',
						redirectUrl: '/departments'
					});
			});

		}]);

	ItExpertApp.controller('positionsController',
		['$rootScope', '$scope', '$http', 'growl', function ($rootScope, $scope, $http, growl) {

			gridCtrl(
				this,
				$rootScope,
				$scope,
				$http,
				growl,
				{
					getUrl: '/api/positions',
					editUrl: '/positions/edit',
					deleteUrl: '/api/positions/',
					fields: [
						{ caption: 'Должность', name: 'Name' },
						{ caption: 'Оклад', name: 'Salary' },
					]
				});

		}]);

	ItExpertApp.controller('positionController',
		['$rootScope', '$scope', '$routeParams', '$http', 'growl',
			function ($rootScope, $scope, $routeParams, $http, growl) {

				formCtrl(
					this,
					$rootScope,
					$scope,
					$routeParams,
					$http,
					growl,
					{
						action: '/api/positions/',
						redirectUrl: '/positions',
						fields: [
							{ elType: 'input', type: 'text', name: 'Name', label: 'Должность:', required: true },
							{ elType: 'input', type: 'number', name: 'Salary', label: 'Оклад:', required: true }
						]
					});
		}]);

})();