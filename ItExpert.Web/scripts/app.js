﻿
(function (w) {

	var mainNavLinks = document.querySelectorAll('#app-main-nav a'),
		mainViewContainer = $('#app-main-view'),
		$topBlockUI = document.querySelector('.blockUI'),
		$jGrowlContainer = $('#jGrowl-container'),
		requestQueue = [];

	//Custom Block UI

	function showBlockUI() {
		$topBlockUI.style.display = 'block';
	};

	function hideBlockUI() {
		$topBlockUI.style.display = 'none';
	};

	function addToQueue(xhr) {
		showBlockUI();
		requestQueue.push(xhr);
	}

	function refreshQueue() {
		var i = 0;
		while (i < requestQueue.length) {
			if (requestQueue[i].readyState == 4) {
				requestQueue.splice(i, 1);
				continue;
			}
			i++;
		}
		if (requestQueue.length == 0) {
			hideBlockUI();
		}
	}

	$(document)
		.ajaxSend(function (evt, xhr, cfg) {
			if (!cfg.nonblocked) {
				addToQueue(xhr);
			}
		})
		.ajaxStop(refreshQueue);

	//jGrowl

	$.jGrowl.defaults.closerTemplate = '<div class="alert alert-info">Скрыть все</div>';

	//Loading views async

	function loadView(url, requestParameters) {
		if (requestParameters) {
			mainViewContainer._requestParameters = requestParameters;
		}
		else {
			delete mainViewContainer._requestParameters;
		}
		mainViewContainer.load('/views' + url + '?_=' + new Date().getTime());
	};

	function onLoadView() {

		loadView(this.getAttribute('data-href'));
		//
		for (var i = mainNavLinks.length; i--;) {
			mainNavLinks[i].classList.remove('active');
		}
		this.classList.add('active');
	};

	for (var i = mainNavLinks.length; i--;) {
		mainNavLinks[i].onclick = onLoadView;
	}

	//

	//Add API

	w.ItExpert = {
		LoadView: loadView,
		Request: function (key) {
			return mainViewContainer._requestParameters ? mainViewContainer._requestParameters[key] : null;
		},
		Alert: function (msg, type) {
			$jGrowlContainer.jGrowl({
				message: msg,
				group: 'alert-' + type,
				life: 5000
			});
		}
	};

	//Run app

	mainNavLinks[0].click();

})(window);