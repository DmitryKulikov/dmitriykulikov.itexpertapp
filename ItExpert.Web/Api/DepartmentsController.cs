﻿
namespace ItExpert.Web.Api
{
	using ItExpert.BO;
	using ItExpert.BL;
	using ItExpert.Sys.Data;

	public class DepartmentsController : CRUDController<Department>
	{
		DepartmentRepository _repo = new DepartmentRepository();

		protected override IRepository<Department> Repo
		{
			get { return _repo; }
		}
	}
}