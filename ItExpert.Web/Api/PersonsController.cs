﻿
namespace ItExpert.Web.Api
{
	using ItExpert.BO;
	using ItExpert.BL;
	using ItExpert.Sys.Data;

	public class PersonsController : CRUDController<Person>
	{
		PersonRepository _repo = new PersonRepository();

		protected override IRepository<Person> Repo
		{
			get { return _repo; }
		}
	}
}