﻿
namespace ItExpert.Web.Api
{
	using ItExpert.BO;
	using ItExpert.BL;
	using ItExpert.Sys.Data;

	public class UsersController : CRUDController<User>
	{
		UserRepository _repo = new UserRepository();

		protected override IRepository<User> Repo
		{
			get { return _repo; }
		}
	}
}