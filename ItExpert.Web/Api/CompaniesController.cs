﻿
namespace ItExpert.Web.Api
{
	using ItExpert.BO;
	using ItExpert.BL;
	using ItExpert.Sys.Data;

	public class CompaniesController : CRUDController<Company>
	{
		CompanyRepository _repo = new CompanyRepository();

		protected override IRepository<Company> Repo
		{
			get { return _repo; }
		}
	}
}