﻿
namespace ItExpert.Web.Api
{
	using ItExpert.BO;
	using ItExpert.BL;
	using ItExpert.Sys.Data;

	public class PositionsController : CRUDController<Position>
	{
		PositionRepository _repo = new PositionRepository();

		protected override IRepository<Position> Repo
		{
			get { return _repo; }
		}
	}
}