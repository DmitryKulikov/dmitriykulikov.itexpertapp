
BEGIN TRANSACTION;

CREATE TABLE Companies (
	Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	Name nvarchar(100) NOT NULL,
	Logo nvarchar(500) NOT NULL
);

CREATE TABLE Departments (
	Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	Name nvarchar(100) NOT NULL,
	CompanyId int NOT NULL REFERENCES Companies(Id)
);

CREATE TABLE Positions (
	Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	Name nvarchar(100) NOT NULL,
	Salary numeric
);

CREATE TABLE Persons (
	Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	FirstName nvarchar(100) NOT NULL,
	LastName nvarchar(100) NOT NULL,
	MiddleName nvarchar(100) NOT NULL,
	Email nvarchar(100) NOT NULL,
	PhoneNumber nvarchar(20) NULL
);

CREATE TABLE Users (
	Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	NickName nvarchar(100) NOT NULL,
	DepartmentId int REFERENCES Departments(Id),
	PersonId int REFERENCES Persons(Id),
	PositionId int REFERENCES Positions(Id),
	SuperUserId int REFERENCES Users(Id)
);

COMMIT;

