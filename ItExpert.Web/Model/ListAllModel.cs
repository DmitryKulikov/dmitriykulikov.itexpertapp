﻿
namespace ItExpert.Web.Model
{
	public class ListAllModel
	{
		public int Offset { get; set; }

		public int RowCount { get; set; }
	}
}