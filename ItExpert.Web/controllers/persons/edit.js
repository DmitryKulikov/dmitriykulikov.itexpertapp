﻿
(function () {

	new ItExpert.UI.Form({
		selector: '[data-id="persons-edit-view-container"]',
		getUrl: '/api/persons/',
		redirectUrl: '/persons/all.html',
		action: '/api/persons/',
		fields: [
			{ elType: 'input', type: 'text', name: 'FirstName', label: 'Имя:', required: true },
			{ elType: 'input', type: 'text', name: 'LastName', label: 'Фамилия:', required: true },
			{ elType: 'input', type: 'text', name: 'MiddleName', label: 'Псевдоним:', required: true },
			{ elType: 'input', type: 'email', name: 'Email', label: 'Email:', required: true },
			{ elType: 'input', type: 'text', name: 'PhoneNumber', label: 'Телефон:', required: true }
		]
	});

})();