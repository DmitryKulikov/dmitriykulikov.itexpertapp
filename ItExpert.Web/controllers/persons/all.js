﻿
(function () {

	new ItExpert.UI.Grid({
		selector: '[data-id="persons-view-container"]',
		getUrl: '/api/persons',
		editUrl: '/persons/edit.html',
		deleteUrl: '/api/persons/',
		fields: [
			{ caption: 'Имя', name: 'FirstName' },
			{ caption: 'Фамилия', name: 'LastName' },
			{ caption: 'Псевдоним', name: 'MiddleName' },
			{ caption: 'Email', name: 'Email' },
			{ caption: 'Тел.', name: 'PhoneNumber' }
		]
	});

})();