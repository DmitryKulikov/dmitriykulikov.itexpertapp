﻿
(function () {

	$.when(
			$.get('/api/persons'),
			$.get('/api/departments'),
			$.get('/api/positions'),
			$.get('/api/users'))
		.then(function (persons,
						departments,
						positions,
						users) {

			persons = persons[0];
			departments = departments[0];
			positions = positions[0];
			users = users[0];

			new ItExpert.UI.Form({
				selector: '[data-id="users-edit-view-container"]',
				getUrl: '/api/users/',
				redirectUrl: '/users/all.html',
				action: '/api/users/',
				fields: [
					{ elType: 'input', type: 'text', name: 'NickName', label: 'Псевдоним:', required: true },
					{
						elType: 'select',
						type: 'select',
						name: 'PersonId',
						label: 'Менеджер:',
						required: true,
						onInit: function (el) {
							var i, item;
							for (i = 0; item = persons[i]; i++) {
								el.innerHTML += '<option value="' + item.Id + '">' + item.FirstName + ' ' + item.LastName + '</option>';
							}
						}
					},
					{
						elType: 'select',
						type: 'select',
						name: 'DepartmentId',
						label: 'Департамент:',
						required: true,
						onInit: function (el) {
							var i, item;
							for (i = 0; item = departments[i]; i++) {
								el.innerHTML += '<option value="' + item.Id + '">' + item.Name + '</option>';
							}
						}
					},
					{
						elType: 'select',
						type: 'select',
						name: 'PositionId',
						label: 'Должность:',
						required: true,
						onInit: function (el) {
							var i, item;
							for (i = 0; item = positions[i]; i++) {
								el.innerHTML += '<option value="' + item.Id + '">' + item.Name + '</option>';
							}
						}
					},
					{
						elType: 'select',
						type: 'select',
						name: 'SuperUserId',
						label: 'Супер-пользователь:',
						required: true,
						onInit: function (el) {
							var i, item;
							for (i = 0; item = users[i]; i++) {
								el.innerHTML += '<option value="' + item.Id + '">' + item.NickName + '</option>';
							}
						}
					}
				]
			});
		},
		function () {
			ItExpert.LoadView('/users/all.html');
		});

})();