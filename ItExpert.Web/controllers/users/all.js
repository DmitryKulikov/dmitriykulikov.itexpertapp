﻿
(function () {

	new ItExpert.UI.Grid({
		selector: '[data-id="users-view-container"]',
		getUrl: '/api/users',
		editUrl: '/users/edit.html',
		deleteUrl: '/api/users/',
		fields: [
			{ caption: 'Ник', name: 'NickName' },
			{ caption: 'Менеджер', name: 'PersonName' },
			{ caption: 'Департамент', name: 'DepartmentName' },
			{ caption: 'Должность', name: 'PositionName' },
			{ caption: 'Супер-пользователь', name: 'SuperUserName' }
		]
	});

})();