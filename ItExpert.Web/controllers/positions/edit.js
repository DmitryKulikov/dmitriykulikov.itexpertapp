﻿
(function () {

	new ItExpert.UI.Form({
		selector: '[data-id="positions-edit-view-container"]',
		getUrl: '/api/positions/',
		redirectUrl: '/positions/all.html',
		action: '/api/positions/',
		fields: [
			{ elType: 'input', type: 'text', name: 'Name', label: 'Должность:', required: true },
			{ elType: 'input', type: 'number', name: 'Salary', label: 'Оклад:', required: true }
		]
	});

})();