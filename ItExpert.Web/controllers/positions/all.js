﻿
(function () {

	new ItExpert.UI.Grid({
		selector: '[data-id="positions-view-container"]',
		getUrl: '/api/positions',
		editUrl: '/positions/edit.html',
		deleteUrl: '/api/positions/',
		fields: [
			{ caption: 'Должность', name: 'Name' },
			{ caption: 'Оклад', name: 'Salary' },
		]
	});

})();