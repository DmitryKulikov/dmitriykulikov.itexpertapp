﻿
(function () {

	new ItExpert.UI.Grid({
		selector: '[data-id="companies-view-container"]',
		getUrl: '/api/companies',
		editUrl: '/companies/edit.html',
		deleteUrl: '/api/companies/',
		fields: [
			{ caption: 'Название', name: 'Name' },
			{ caption: 'Логотип', name: 'Logo' },
		]
	});

})();