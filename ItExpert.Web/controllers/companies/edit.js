﻿
(function () {

	new ItExpert.UI.Form({
		selector: '[data-id="companies-edit-view-container"]',
		getUrl: '/api/companies/',
		redirectUrl: '/companies/all.html',
		action: '/api/companies/',
		fields: [
			{ elType: 'input', type: 'text', name: 'Name', label: 'Название:', required: true },
			{ elType: 'input', type: 'text', name: 'Logo', label: 'Логотип:', required: true }
		]
	});

})();