﻿
(function () {

	$.ajax({
		method: 'get',
		url: '/api/companies',
		success: function (r) {
			new ItExpert.UI.Form({
				selector: '[data-id="departments-edit-view-container"]',
				getUrl: '/api/departments/',
				redirectUrl: '/departments/all.html',
				action: '/api/departments/',
				fields: [
					{ elType: 'input', type: 'text', name: 'Name', label: 'Название:', required: true },
					{
						elType: 'select',
						type: 'select',
						name: 'CompanyId',
						label: 'Организация:',
						required: true,
						onInit: function (el) {
							var i, item;
							for (i = 0; item = r[i]; i++) {
								el.innerHTML += '<option value="' + item.Id + '">' + item.Name + '</option>';
							}
						}
					}
				]
			});
		},
		error: function () {
			ItExpert.LoadView('/departments/all.html');
		}
	});

})();