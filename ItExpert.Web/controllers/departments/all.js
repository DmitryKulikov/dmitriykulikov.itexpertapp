﻿
(function () {

	new ItExpert.UI.Grid({
		selector: '[data-id="departments-view-container"]',
		getUrl: '/api/departments',
		editUrl: '/departments/edit.html',
		deleteUrl: '/api/departments/',
		fields: [
			{ caption: 'Название', name: 'Name' },
			{ caption: 'Организация', name: 'CompanyName' }
		]
	});

})();